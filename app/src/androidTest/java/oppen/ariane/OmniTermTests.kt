package oppen.ariane

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class OmniTermTests {

    var address: String? = null

    @Test
    fun omniTestNavigation(){

        val omni = OmniTerm(object: OmniTerm.Listener{
            override fun request(_address: String) {
                address = _address
            }

            override fun openBrowser(address: String) {
                //
            }
        })

        omni.navigation("gemini://oppen.gemini")
        Truth.assertThat(address).isEqualTo("gemini://oppen.gemini")

        omni.navigation("software/")
        Truth.assertThat(address).isEqualTo("gemini://oppen.gemini/software/")

        omni.navigation("/")
        Truth.assertThat(address).isEqualTo("gemini://oppen.gemini")
    }

    @Test
    fun omniTestSearch(){

        val omni = OmniTerm(object: OmniTerm.Listener{
            override fun request(_address: String) {
                address = _address
            }

            override fun openBrowser(address: String) {
                //
            }
        })

        omni.input("oppen.gemini")
        Truth.assertThat(address).isEqualTo("gemini://oppen.gemini")

        omni.input("oppen gemini")
        Truth.assertThat(address).isEqualTo("gemini://gus.guru/search?oppen+gemini")
    }

    @Test
    fun omniTestRelative(){

        val omni = OmniTerm(object: OmniTerm.Listener{
            override fun request(_address: String) {
                address = _address
            }

            override fun openBrowser(address: String) {
                //
            }
        })

        omni.input("oppen.gemini/debug/")
        Truth.assertThat(address).isEqualTo("gemini://oppen.gemini/debug/")

        omni.navigation("../")
        Truth.assertThat(address).isEqualTo("gemini://oppen.gemini")
    }

}