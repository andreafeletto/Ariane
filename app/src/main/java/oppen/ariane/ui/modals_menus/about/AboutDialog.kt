package oppen.ariane.ui.modals_menus.about

/**
 * Copyright © 2020 Öppenlab oppenlab.net
 */

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.app.AppCompatDialog
import androidx.appcompat.widget.AppCompatTextView
import kotlinx.android.synthetic.main.dialog_about.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import oppen.ariane.BuildConfig
import oppen.ariane.R
import java.lang.StringBuilder
import java.security.SecureRandom
import java.security.Security
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocket
import javax.net.ssl.SSLSocketFactory

object AboutDialog {

    fun show(context: Context){
        val dialog = AppCompatDialog(context, R.style.AppTheme)

        val view = View.inflate(context, R.layout.dialog_about, null)
        dialog.setContentView(view)

        view.close_tab_dialog.setOnClickListener {
            dialog.dismiss()
        }

        view.version_label.text = BuildConfig.VERSION_NAME

        view.oppenlab_button.setOnClickListener {
            context.startActivity(Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse("https://oppen.digital")
            })
        }

        buildTLSInfo(view.tls_info)


        dialog.show()
    }

    private fun buildTLSInfo(tlsInfo: AppCompatTextView?) {
        GlobalScope.launch {
            val sb = StringBuilder()

            sb.append("\n")
            sb.append("Supported Keystores on this device:\n\n")
            Security.getProviders().forEach { secProvider ->
                println("${secProvider.name} version: ${secProvider.version} info: ${secProvider.info}")

                val supKeystores = secProvider.services.filter {  service ->
                    service.type == "KeyStore"
                }

                if(supKeystores.isNotEmpty()){
                    sb.append("${secProvider.name} version: ${secProvider.version}\n")
                    supKeystores.forEach { keystore ->
                        sb.append("     ${keystore.algorithm}\n")
                    }
                }
            }

            sb.append("\n")

            val sslContext = SSLContext.getInstance("TLS")
            sslContext.init(null, null, SecureRandom())
            val factory: SSLSocketFactory = sslContext.socketFactory
            val socket = factory.createSocket() as SSLSocket

            sb.append("\n")
            sb.append("Supported TLS protocols on this device:\n\n")
            socket.supportedProtocols.forEach { protocol ->
                sb.append("$protocol \n")
            }

            sb.append("\n\n")

            sb.append("Enabled cipher suites on this device:\n\n")
            val defaultCipherSuites = factory.defaultCipherSuites
            defaultCipherSuites.forEach { cipherSuite ->
                sb.append("$cipherSuite \n")
            }

            sb.append("\n")
            Handler(Looper.getMainLooper()).post{
                tlsInfo?.text = sb.toString()
            }
        }
    }
}